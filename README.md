# Dashboard Collaborative Consumption in Germany
https://app.powerbi.com/view?r=eyJrIjoiZmE0ZGVjMTktOWY2Ni00Yzc3LTg2ZmMtNWNjYjEyNzA5YTgzIiwidCI6IjE5ZWRjNjZhLWI5NzAtNDk4My1iZjk4LWNhZDZiMzljMzI5YiIsImMiOjl9

# Dashboard which visualizes temperature data which is (manually) written into the database
https://app.powerbi.com/view?r=eyJrIjoiNzBhZmIzNzgtOGJhZi00ZjA2LWE1NDktMWQwNTRhOWJmMTA4IiwidCI6IjIxNGQxYmQ4LTQ4ZjYtNDc3NS1iYzFhLWQwMzI3MmIxYWM0NSIsImMiOjh9

# Dashboard which receives and evaluates temperature sensor data
https://app.powerbi.com/groups/me/reports/7aa5e194-6190-4431-b0b3-8c53a5113965/ReportSectione116540024d044ea4b67

# Temperature Dashboard which was used as a mockup
https://app.powerbi.com/view?r=eyJrIjoiYzg2NjAyNjQtYTUwMi00ZmE5LWI2MjgtN2YwMTZmODI5ZjAyIiwidCI6IjIxNGQxYmQ4LTQ4ZjYtNDc3NS1iYzFhLWQwMzI3MmIxYWM0NSIsImMiOjh9

# Dashboard which further evaluates sensory temperature data

## Version 1
https://app.powerbi.com/view?r=eyJrIjoiYmUzZDBjNTMtNmU0MC00ZWFiLTljNTEtNWZiNTU0NjRmMDgzIiwidCI6IjIxNGQxYmQ4LTQ4ZjYtNDc3NS1iYzFhLWQwMzI3MmIxYWM0NSIsImMiOjh9

## Version 2
https://app.powerbi.com/view?r=eyJrIjoiOTc5YjRkZjQtYmEwOC00Mzk3LThhY2MtMzA1ZTFlZjY1NjY3IiwidCI6IjIxNGQxYmQ4LTQ4ZjYtNDc3NS1iYzFhLWQwMzI3MmIxYWM0NSIsImMiOjh9





